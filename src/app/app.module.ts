import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {routing} from './app.routes';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BreezeBridgeAngularModule } from 'breeze-bridge-angular';
import { HttpModule } from '@angular/http';
import { DxDataGridModule, DxScrollViewModule, DxListModule, DxDateBoxModule} from 'devextreme-angular';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';




import { AppComponent } from './app.component';
import { DataModelComponent } from './main/data-model/data-model.component';
import { CustomerModalComponent } from './modals/customer-modal/customer-modal.component';
import { DataModelService } from './main/data-model.service';
import { ConfirmModalComponent } from './modals/confirm-modal/confirm-modal.component';
import { InputModalComponent } from './modals/input-modal/input-modal.component';
import { RequestModalComponent } from './modals/request-modal/request-modal.component';
import { TopicModalComponent } from './modals/topic-modal/topic-modal.component';
import { LoginComponent } from './auth/login/login.component';
import { AccountService } from './auth/account.service';
import { AccountGuard } from './auth/account.guard';
import { HeaderComponent } from './header/header.component';
import { RegisterModalComponent } from './modals/register-modal/register-modal.component';
import { QueryModalComponent } from './modals/query-modal/query-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    DataModelComponent,
    CustomerModalComponent,
    ConfirmModalComponent,
    InputModalComponent,
    RequestModalComponent,
    TopicModalComponent,
    LoginComponent,
    HeaderComponent,
    RegisterModalComponent,
    QueryModalComponent,
  ],
  imports: [
    BrowserModule, routing,
    FlexLayoutModule,
    NgbModule.forRoot(),
    FormsModule, ReactiveFormsModule,
    BreezeBridgeAngularModule, HttpModule,
    DxDataGridModule, DxScrollViewModule, DxListModule, DxDateBoxModule,
    FileUploadModule
  ],
  providers: [DataModelService, AccountService, AccountGuard],
  bootstrap: [AppComponent],
  entryComponents: [CustomerModalComponent, RequestModalComponent, TopicModalComponent, RegisterModalComponent, QueryModalComponent]
})
export class AppModule { }
