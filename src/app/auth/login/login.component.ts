import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: any = {};
  email: string;
  password: string;

  constructor(private service: AccountService, private router: Router) { }

  ngOnInit() {

  }

  login() {
    console.log(1);
    this.user.email = this.email;
    this.user.password = this.password;
    this.service.login(this.user).then(res => {
      console.log(res);
      console.log(this.user);
      this.router.navigate(['/model']);
    }).catch(error => console.log(error));
  }

}
