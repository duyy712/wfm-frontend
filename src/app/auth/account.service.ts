import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AccountService {
  // urlApi = 'http://localhost:44366/api';
  urlApi = 'http://192.168.11.48:5000/api';
  isLoggedIn = false;
  private loggedInSrc = new BehaviorSubject<boolean>(false);
  loggedIn$ = this.loggedInSrc.asObservable();
  

  constructor(private http: Http) {
    this.isLoggedIn = !!localStorage.getItem('auth_token');
    this.loggedInSrc.next(this.isLoggedIn);
  }

  register(user) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    // let options = new RequestOptions({Headers: headers});
    const body = JSON.stringify(user);
    return this.http.post(this.urlApi + '/account', body, { headers: headers })
      .toPromise()
      .then(res => res.json())
      .catch(error => Promise.reject(error));
  }

  login(user) {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const body = JSON.stringify(user);
    return this.http.post(this.urlApi + '/auth/login', body, { headers: headers })
    .toPromise()
    .then(res => res.json()).then(res => {
      this.isLoggedIn = true;
      this.loggedInSrc.next(true);
      localStorage.setItem('auth_token', res.auth_token);
      console.log(res);

    })
    .catch(error => Promise.reject(error));
  }

  logout() {
    localStorage.removeItem('auth_token');
    this.isLoggedIn = false;
    this.loggedInSrc.next(false);
  }

}
