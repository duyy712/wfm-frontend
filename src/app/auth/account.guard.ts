import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AccountService } from './account.service';


@Injectable()
export class AccountGuard implements CanActivate {
  constructor(private service: AccountService, private router: Router) { }

  canActivate() {
    if (!this.service.isLoggedIn) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
