import { Routes, RouterModule } from '@angular/router';
import { DataModelComponent } from './main/data-model/data-model.component';
import { LoginComponent } from './auth/login/login.component';
import { AccountGuard } from './auth/account.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'model',
    pathMatch: 'full'
  },
  {
    path: 'model',
    component: DataModelComponent,
    // canActivate: [AccountGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

export const routing = RouterModule.forRoot(routes);
