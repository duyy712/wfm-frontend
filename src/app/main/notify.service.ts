import { Injectable } from '@angular/core';
import notify from 'devextreme/ui/notify';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from '../modals/confirm-modal/confirm-modal.component';
import { InputModalComponent } from '../modals/input-modal/input-modal.component';

@Injectable()
export class NotifyService {
  constructor(private modalService: NgbModal) { }

  toast(message: string, type: string, duration?: number) {
    notify({
      animation: {
        show: { type: 'pop', from: { opacity: 1, scale: 0 }, to: { scale: 1 } },
        hide: { type: 'pop', from: { scale: 1 }, to: { scale: 0 } }
      },
      displayTime: duration || 5000,
      message: message,
      type: type || 'info'
    });
  }
  toastSuccess(message) {
    this.toast(message, 'success');
  }
  toastError(message) {
    this.toast(message, 'error');
  }
  toastInfo(message) {
    this.toast(message, 'info');
  }
  toastWarning(message) {
    this.toast(message, 'warning');
  }

  confirm(title, message): Promise<any> {
    const modalRef = this.modalService.open(ConfirmModalComponent);
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
    return modalRef.result;
  }

  input(options): Promise<any> {
    const modalRef = this.modalService.open(InputModalComponent);
    modalRef.componentInstance.options = options;
    return modalRef.result;
  }
}
