import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CustomerModalComponent } from '../../modals/customer-modal/customer-modal.component';
import { DataModelService } from '../data-model.service';
import { RequestModalComponent } from '../../modals/request-modal/request-modal.component';
import { TopicModalComponent } from '../../modals/topic-modal/topic-modal.component';
import { QueryModalComponent } from '../../modals/query-modal/query-modal.component';
import { Predicate } from 'breeze-client';


@Component({
  selector: 'app-data-model',
  templateUrl: './data-model.component.html',
  styleUrls: ['./data-model.component.css']
})
export class DataModelComponent implements OnInit {
  itemControl = new FormControl();
  customers: any[];
  customer: any;
  topics: any[];
  topic: any;
  requests: any[];
  searchText: any;


  constructor(private modal: NgbModal, private service: DataModelService) { }

  ngOnInit() {
    this.service.fetchMetadata().then(() => {
      this.service.sortCustomers().then(data => this.customers = data[0]);
    });
    this.itemControl.valueChanges.subscribe(value => {
      const p = Predicate.create('Name', 'Contains', value);
      this.service.query(p, 'Customers').then(res => this.customers = res);
    });
  }

  getCustomers() {
    const p = Predicate.create('Name', 'Contains', this.searchText);
    this.service.query(p, 'Customers').then(res => this.customers = res);
  }

  addCustomer() {
    const entity = this.service.createCustomer();
    const modalRef = this.modal.open(CustomerModalComponent);
    modalRef.componentInstance.customer = entity;
    modalRef.componentInstance.isEditing = false;
    modalRef.result.then(res => {
      this.customers.push(res);
    }).catch(e => { });
  }

  editCustomer(customer) {
    const modalRef = this.modal.open(CustomerModalComponent);
    modalRef.componentInstance.customer = customer;
    modalRef.componentInstance.isEditing = true;

    modalRef.result.then(res => {
      this.service.saveChanges();

    }).catch(e => console.log(e));

  }

  selectCustomer(customer) {
    this.customer = customer;
    this.topic = null;
    this.service.getTopicsByCustomer(customer.Id).then(res => {
      this.topics = res;
      res.forEach(el => console.log(el.Name));
    });
  }

  addTopic() {
    const modalRef = this.modal.open(TopicModalComponent);
    const entity = this.service.createTopic();
    modalRef.componentInstance.topic = entity;
    modalRef.componentInstance.topic.CustomerId = this.customer.Id;
    modalRef.result.then(res => {
      this.topics.push(res);
    }).catch(e => console.log(e));


  }

  selectTopic(topic) {
    this.topic = topic;
    this.service.getRequestByTopic(topic.Id).then(res => {
      console.log(res);
      this.requests = res;
    });
  }

  selectRequest(value: number) {
    this.service.getRequestById(value).then(res => {
      const modalRef = this.modal.open(RequestModalComponent);
      modalRef.componentInstance.request = res[0];
      modalRef.componentInstance.isEditing = true;

    });
  }

  addRequest() {
    const entity = this.service.createCustomerRequest();
    const modalRef = this.modal.open(RequestModalComponent);
    modalRef.componentInstance.request = entity;
    modalRef.componentInstance.isEditing = false;
    modalRef.componentInstance.request.TopicId = this.topic.Id;
  }

  searchRequest() {
    const modalRef = this.modal.open(QueryModalComponent);
    modalRef.componentInstance.topicId = this.topic.Id;
  }
}
