import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AccountService } from '../auth/account.service';
import { RegisterModalComponent } from '../modals/register-modal/register-modal.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  status: boolean;

  constructor(private modal: NgbModal, private service: AccountService) { }

  ngOnInit() {
    this.service.loggedIn$.subscribe(res => this.status = res);
  }

  register() {
    const modalRef = this.modal.open(RegisterModalComponent);
    modalRef.result.then(res => {
      console.log('success');
    }).catch(error => console.log('error'));
  }

  logout() {
    this.service.logout();
  }

}
