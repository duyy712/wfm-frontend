import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-input-modal',
  templateUrl: './input-modal.component.html',
  styleUrls: ['./input-modal.component.css']
})
export class InputModalComponent implements OnInit {
  @Input() options: any;
  value = '';

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.value = this.options.initialValue;
  }

}
