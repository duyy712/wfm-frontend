import { Component, OnInit, Input } from '@angular/core';
import { DataModelService } from '../../main/data-model.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';

@Component({
  selector: 'app-topic-modal',
  templateUrl: './topic-modal.component.html',
  styleUrls: ['./topic-modal.component.css']
})
export class TopicModalComponent implements OnInit {
  @Input() topic: any;

  constructor(private service: DataModelService, private modal: NgbActiveModal) { }

  ngOnInit() { }


  submit() {
    this.service.saveChanges([this.topic]).then(res => {
      this.modal.close(res[0]);
    }).catch(e => this.modal.dismiss());
  }

  dismiss() {
    this.service.rejectChanges();
    this.modal.dismiss();
  }

}
