import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataModelService } from '../../main/data-model.service';
import { Predicate } from 'breeze-client';



@Component({
  selector: 'app-query-modal',
  templateUrl: './query-modal.component.html',
  styleUrls: ['./query-modal.component.css']
})
export class QueryModalComponent implements OnInit {

  @Input() topicId: any;
  fromEmployee: string;
  toEmployee: string;
  doEmployee: string;
  p1: Predicate;
  p2: Predicate;
  p3: Predicate;
  p4: Predicate;

  constructor(private service: DataModelService, private modal: NgbModal) { }

  ngOnInit() {
  }

  submit() {
    if (this.fromEmployee && this.fromEmployee.length > 0) {
      this.p1 = Predicate.create('FromEmployeeCode', '==', this.fromEmployee);
    }
    if (this.toEmployee && this.toEmployee.length > 0) {
      this.p2 = Predicate.create('ToEmployeeCode', '==', this.toEmployee);
    }
    if (this.doEmployee && this.doEmployee.length > 0) {
      this.p3 = Predicate.create('DoEmployeeCode', '==', this.doEmployee);
    }
    this.p4 = Predicate.create('TopicId', '==', this.topicId);

    if (this.p1) {
      this.p4 = this.p4.and(this.p1);
    }
    if (this.p2) {
      this.p4 = this.p4.and(this.p2);
    }
    if (this.p3) {
      this.p4 = this.p4.and(this.p3);
    }

    this.service.query(this.p4, 'CustomerRequests').then(res => {
      console.log(res);
    });




  }

}
