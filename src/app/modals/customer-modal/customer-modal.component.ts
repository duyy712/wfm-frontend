import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataModelService } from '../../main/data-model.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-customer-modal',
  templateUrl: './customer-modal.component.html',
  styleUrls: ['./customer-modal.component.css']
})
export class CustomerModalComponent implements OnInit {
  form: FormGroup;
  title: string;

  @Input() customer: any;
  @Input() isEditing: boolean;

  constructor(private service: DataModelService, private fb: FormBuilder, private modal: NgbActiveModal) { }

  ngOnInit() {
    this.title = this.isEditing ? 'Edit Customer' : 'New Customer';
  }

  submit() {
    this.service.saveChanges([this.customer]).then(res => {
      this.modal.close(res[0]);
    }).catch(e => this.modal.dismiss());
  }

  dismiss() {
    this.service.rejectChanges();
    this.modal.dismiss();
  }

}
