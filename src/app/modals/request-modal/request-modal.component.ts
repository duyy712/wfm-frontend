import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataModelService } from '../../main/data-model.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { FileItem, ParsedResponseHeaders, FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'app-request-modal',
  templateUrl: './request-modal.component.html',
  styleUrls: ['./request-modal.component.css']
})
export class RequestModalComponent implements OnInit {
  title: string;
  uploader: any;

  @Input() request: any;
  @Input() isEditing: boolean;

  constructor(private service: DataModelService, private fb: FormBuilder, private modal: NgbActiveModal) { }

  ngOnInit() {
    this.title = this.isEditing ? 'Edit Request' : 'New Request';
    this.uploader = new FileUploader({ url: 'http://localhost:61028/breeze/datamodel/import' });

  }

  submit() {
    this.service.saveChanges([this.request]).then(res => {
      this.modal.close(res[0]);
    }).catch(e => this.modal.dismiss());
  }

  dismiss() {
    this.service.rejectChanges();
    this.modal.dismiss();
  }

  uploadFile() {
    const file = { FileName: '', FilePath: '' };
    this.upload().then(task => {
      file.FileName = task[0].FileName;
      file.FilePath = task[0].FilePath;
      const entity = this.service.createSourceFile(file);
      this.service.saveChanges([entity]).then(res => this.request.DescriptionFileId = res[0].Id);
    }).catch(error => console.log(error));
  }
  upload(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.uploader.onSuccessItem = (item: FileItem, response: string, status: number, headers: ParsedResponseHeaders) =>
        resolve(JSON.parse(response));

      this.uploader.onErrorItem = (item: FileItem, response: string, status: number, headers: ParsedResponseHeaders) => {
        reject();
      };
      this.uploader.uploadAll();
    });
  }
}
