import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap/modal/modal-ref';
import { AccountService } from '../../auth/account.service';

@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.css']
})
export class RegisterModalComponent implements OnInit {
  email: string;
  password: string;
  confirmPassword: string;
  firstName: string;
  lastName: string;

  constructor(private modal: NgbActiveModal, private service: AccountService) { }

  ngOnInit() {
  }

  submit() {
    const user = {
      email: this.email,
      password: this.password,
      confirmPassword: this.confirmPassword,
      firstName: this.firstName,
      lastName: this.lastName
    };
    this.service.register(user).then(res => {
      console.log(res);
      this.modal.close(res);
    });

  }

}
